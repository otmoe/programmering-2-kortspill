package org.OleThomas;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for PlayingCard class.
 *
 * @see PlayingCard
 * @author Ole-Thomas
 * @version 1.0
 */
class PlayingCardTest {

  /** Positive test for playingCard */
  @Test
  void posPlayingCard() {
    PlayingCard card1 = new PlayingCard('S', 12);

    assertEquals(card1.getSuit(), 'S');
    assertEquals(card1.getFace(), 12);
  }

  /** Negative test for playingCard */
  @Test
  void negPlayingCard() {
    PlayingCard card1 = new PlayingCard('S', 12);

    assertNotEquals(card1.getSuit(), 'H');
    assertNotEquals(card1.getFace(), 15);
  }

  /** Positive test for getAsString */
  @Test
  void posGetAsString() {
    PlayingCard card1 = new PlayingCard('S', 12);
    assertEquals(card1.getAsString(), "S12");
  }

  /** Negative test for getAsString */
  @Test
  void negGetAsString() {
    PlayingCard card1 = new PlayingCard('S', 12);
    assertNotEquals(card1.getAsString(), "H13");
  }

  /** Positive test for equals */
  @Test
  void posTestEquals() {
    PlayingCard card1 = new PlayingCard('S', 12);
    PlayingCard card2 = new PlayingCard('S', 12);
    assertTrue(card1.equals(card2));
    assertTrue(card2.equals(card1));
  }

  /** Negative test for equals */
  @Test
  void negTestEquals() {
    PlayingCard card1 = new PlayingCard('S', 12);
    PlayingCard card2 = new PlayingCard('H', 13);
    assertFalse(card1.equals(card2));
    assertFalse(card2.equals(card1));
  }

  /** Positive test for hashCode. */
  @Test
  void testHashCode() {
    PlayingCard card1 = new PlayingCard('S', 12);
    assertEquals(card1.hashCode(), 9312);
  }
}
