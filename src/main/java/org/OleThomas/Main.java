package org.OleThomas;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Main application of the program
 */
public class Main extends Application {

  Stage window;
  Button dealHand;
  Button checkHand;
  DeckOfCards deck;
  Hand hand;
  Text handText;
  Text containsFiveFlush;
  Text queenOfSpades;
  Text containsHearts;
  Text sumOfTheFaces;
  Text containsFiveFlushText;
  Text queenOfSpadesText;
  Text containsHeartsText;
  Text sumOfTheFacesText;

  @Override
  public void start(Stage primaryStage) {
    try {
      window = primaryStage;

      Group root = new Group();
      Scene scene = new Scene(root, 900, 700, Color.WHITE);

      deck = new DeckOfCards();
      hand = new Hand(5, deck);

      handText = new Text("None");
      handText.setLayoutX(150);
      handText.setLayoutY(200);
      handText.setFont(Font.font(50));

      containsFiveFlushText = new Text("Flush:");
      containsFiveFlushText.setLayoutX(100);
      containsFiveFlushText.setLayoutY(600);

      queenOfSpadesText = new Text("Queen of spades:");
      queenOfSpadesText.setLayoutX(400);
      queenOfSpadesText.setLayoutY(600);

      containsHeartsText = new Text("Cards of hearts:");
      containsHeartsText.setLayoutX(400);
      containsHeartsText.setLayoutY(450);

      sumOfTheFacesText = new Text("Sum of the faces:");
      sumOfTheFacesText.setLayoutX(100);
      sumOfTheFacesText.setLayoutY(450);

      dealHand = new Button("Deal Hand");
      dealHand.setLayoutX(700);
      dealHand.setLayoutY(150);
      dealHand.setOnAction(
          eventDeal -> {
            this.hand = new Hand(5, deck);
            deck.shuffleDeck();
            handText.setText(hand.toString());
          });

      containsFiveFlush = new Text("None");
      containsFiveFlush.setLayoutX(140);
      containsFiveFlush.setLayoutY(600);

      queenOfSpades = new Text("None");
      queenOfSpades.setLayoutX(500);
      queenOfSpades.setLayoutY(600);

      containsHearts = new Text("None");
      containsHearts.setLayoutX(500);
      containsHearts.setLayoutY(450);

      sumOfTheFaces = new Text("None");
      sumOfTheFaces.setLayoutX(200);
      sumOfTheFaces.setLayoutY(450);

      checkHand = new Button("Check Hand");
      checkHand.setLayoutX(700);
      checkHand.setLayoutY(200);
      checkHand.setOnAction(
          eventCheck -> {
            containsFiveFlush.setText(String.valueOf(hand.containsFiveFlush()));
            containsHearts.setText(hand.getHearts());
            queenOfSpades.setText(String.valueOf(hand.containsQueenOfSpade()));
            sumOfTheFaces.setText(String.valueOf(hand.sumOfTheFaces()));
          });

      root.getChildren().add(dealHand);
      root.getChildren().add(checkHand);
      root.getChildren().add(containsFiveFlush);
      root.getChildren().add(queenOfSpades);
      root.getChildren().add(containsHearts);
      root.getChildren().add(sumOfTheFaces);
      root.getChildren().add(containsFiveFlushText);
      root.getChildren().add(queenOfSpadesText);
      root.getChildren().add(containsHeartsText);
      root.getChildren().add(sumOfTheFacesText);
      root.getChildren().add(handText);

      window.setTitle("Card Game");
      window.setResizable(false);
      window.setScene(scene);
      window.show();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    launch();
  }
}
