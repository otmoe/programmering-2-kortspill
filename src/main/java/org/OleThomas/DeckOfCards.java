package org.OleThomas;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a deck of playing cards.
 *
 * @see PlayingCard
 * @author Ole-Thomas
 * @version 1.0
 */
public class DeckOfCards {

  private ArrayList<PlayingCard> deckOfCards;
  private char[] suits = {'S', 'H', 'D', 'C'};

  /** Creates an instance of a deck of cards with all cards accounted for. */
  public DeckOfCards() {
    deckOfCards = new ArrayList<PlayingCard>();
    shuffleDeck();
  }

  /** "Shuffels" a deck of playing cards to get all used cards back. */
  public void shuffleDeck() {
    deckOfCards.clear();
    PlayingCard tmp = null;
    for (char suit : suits) {
      for (int i = 1; i <= 13; i++) {
        tmp = new PlayingCard(suit, i);
        deckOfCards.add(tmp);
      }
    }
  }

  /**
   * Getter for deckOfCards
   *
   * @return deckOfCards
   */
  public ArrayList<PlayingCard> getDeckOfCards() {
    return deckOfCards;
  }

  /**
   * Getter for deckOfCards size
   *
   * @return deckOfCards.size()
   */
  public int getSize() {
    return deckOfCards.size();
  }
}
