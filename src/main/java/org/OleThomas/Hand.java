package org.OleThomas;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Represents an instance of a hand of cards.
 *
 * @see PlayingCard
 * @author Ole-Thomas
 * @version 1.0
 */
public class Hand {

  private ArrayList<PlayingCard> hand;
  private Random random = new Random();
  private char[] suits = {'S', 'H', 'D', 'C'};

  /**
   * Creates an instance of a hand of cards.
   *
   * @param n number of cards in the hand
   * @param deckOfCards the deck of cards to draw from
   */
  public Hand(int n, DeckOfCards deckOfCards) {
    this.hand = dealHand(n, deckOfCards);
  }

  /**
   * "Draws" a hand of cards
   *
   * @param n number of cards in the hand
   * @param deckOfCards the deck of cards to draw from
   * @return hand
   */
  public ArrayList<PlayingCard> dealHand(int n, DeckOfCards deckOfCards) {
    if (n < 1 || n > deckOfCards.getSize()) {
      throw new IllegalArgumentException("Invalid number of cards to deal");
    }

    ArrayList<PlayingCard> hand = new ArrayList<>();
    ArrayList<PlayingCard> deckCopy = deckOfCards.getDeckOfCards();

    while (hand.size() < n) {
      int randIndex = this.random.nextInt(deckCopy.size());
      PlayingCard card = deckCopy.get(randIndex);
      hand.add(card);
      deckCopy.remove(card);
    }
    return hand;
  }

  /**
   * Method to find the sum of the faces in the hand.
   *
   * @return sum
   */
  public int sumOfTheFaces() {
    int sum = hand.stream().mapToInt(card -> card.getFace()).sum();
    return sum;
  }

  /**
   * Method to find the cards with hearts as the face.
   *
   * @return String of cards or "No Hearts" for no hearts
   */
  public String getHearts() {
    List<String> hearts =
        hand.stream()
            .filter(card -> card.getSuit() == 'H')
            .map(card -> String.format("%c%d", card.getSuit(), card.getFace()))
            .collect(Collectors.toList());
    if (hearts.isEmpty()) {
      return "No Hearts";
    } else {
      StringBuilder txt = new StringBuilder();
      for (String string : hearts) {
        txt.append(string);
        txt.append(" ");
      }
      return txt.toString();
    }
  }

  /**
   * Method to find if a hand contains the queen of spades.
   *
   * @return boolean
   */
  public boolean containsQueenOfSpade() {
    boolean queenOfSpades =
        hand.stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    return queenOfSpades;
  }

  /**
   * Method to find if a hand contains a five flush.
   *
   * @return boolean
   */
  public boolean containsFiveFlush() {
    boolean fiveFlush = false;
    for (char suit : suits) {
      long sum = hand.stream().filter(card -> card.getSuit() == suit).count();
      if (sum == 5) {
        fiveFlush = true;
      }
    }
    return fiveFlush;
  }

  /**
   * toString method
   *
   * @return string
   */
  @Override
  public String toString() {
    StringBuilder txt = new StringBuilder();
    for (PlayingCard card : hand) {
      txt.append(card.getAsString());
      txt.append(" ");
    }
    return txt.toString();
  }
}
